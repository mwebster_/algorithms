/*
 * HASH_MAP_02
 * Demonstrates a hash map with ArrayList
*/

/////////////////////////// GLOBALS ////////////////////////////
  
import java.util.Map;
HashMap<Character, ArrayList<Float>> HM = new HashMap<Character, ArrayList<Float>>();
/////////////////////////// SETUP ////////////////////////////

void setup() {
  // adding stuff to our Map
  ArrayList<Float> f = new ArrayList<Float>();
  for(int i=0; i<5; i++){
      float ff = random(50);
      f.add(ff);
  }
  HM.put('H', f);

  
  // accessing the entries
  // Maps are not Lists & order is not a required design
  for(Map.Entry em : HM.entrySet() ) {
    println(em.getKey());
    println(em.getValue());
  }
  
  ArrayList<Float> val2 = HM.get('H');
  println(val2);

}

/////////////////////////// DRAW ////////////////////////////
void draw() {

}

/////////////////////////// FUNCTIONS ////////////////////////////