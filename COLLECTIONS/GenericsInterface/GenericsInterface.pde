/**
 * :::::::::::::::::::
 * ABSTRACT DATA TYPES
 * :::::::::::::::::::
 *
 * Sketch : Generic Interface
 * 
 * Demonstrates an interface for generic collections
 * Note that <E> has been used instead of <Item>
 * <E> is often seen in Java use as a conventional sign
 * for an Element in a collection.
 *
 */

import java.util.Iterator;

Note myNote;

void setup() {
  size(400, 400);
  background(0, 0, 33);
  myNote = new Note();
  myNote.addItem("I will read a book in the sun");
  myNote.addItem("After which I'll watch a film");
  myNote.addItem(500.89);

  String s = (String)myNote.getItem(0);
  float f = (Float)myNote.getItem(2);
  //println( s );
  println( f );
  println( "¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨" );

  //myNote.getAll();
  myNote.getAllIt();
  println(myNote.getType(0));
}


void draw() {
}


class Note<E> implements Collections<E> {
  // in java, E denotes element of a collection
  private E type;
  ArrayList<E> theNote;

  Note() {
    theNote = new ArrayList<E>();
  }

  void addItem(E _e) {
    theNote.add(_e);
    type = _e;
  }

  Object getItem(int _index) {
    return theNote.get(_index);
  }

  void getAll() {
    Iterator<E> itr = theNote.iterator();
    while (itr.hasNext ()) {
      Object e = itr.next();
      println(e + " ");
    }
  }
  
  /**
   * What is the difference between this method 
   * and the method above?
   */
  
  void getAllIt() {    
    for(E e : theNote) {
      println(e + " ");
    }
  }
  
  String getType(int _index) {
    theNote.get(_index);
    return type.getClass().toString();
  }
}


interface Collections<E> {
  void addItem(E _e);
  Object getItem(int _index);
  void getAll();
}