/*
 * HASH_MAP_01
 * Exploring Hash Maps 
 * - Very fast for storing & retrieving. Good for IDs
 * - Not for iterating nor sorting - Lists are for that
 *
 * A Map is a set of associations between a pair of objects
 *
 * MW_3.06.16
 * links / refs ...
 * A HashMap stores a collection of objects, each referenced 
 * by a key. This is similar to an Array, only instead of 
 * accessing elements with a numeric index, a String is used.
 * https://docs.oracle.com/javase/tutorial/collections/interfaces/index.html
 * end

*/

/////////////////////////// GLOBALS ////////////////////////////
  
import java.util.Map;
HashMap<String,Integer> HM = new HashMap<String, Integer>();
HashMap<String,String> HM2 = new HashMap<String, String>();
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(400, 400);
  background(0);
  smooth();
  noStroke();
  // adding stuff to our Map
  HM.put("Mike", 23);
  HM.put("SAM", 73);
  HM.put("Draw", 5);
  
  HM2.put("D", "Mode drawing");
  HM2.put("F", "Mode fooding");
  
  // accessing the entries
  // Maps are not Lists & order is not a required design
  for(Map.Entry em : HM.entrySet() ) {
    println(em.getKey());
    println(em.getValue());
  }
  
  // or by Key
  int val = HM.get("SAM");
  println("SAM"+ " is " +val);
  
  // or by Key
  String val2 = HM2.get("D");
  println("D"+ " is " +val2);

}

/////////////////////////// DRAW ////////////////////////////
void draw() {

}

/////////////////////////// FUNCTIONS ////////////////////////////