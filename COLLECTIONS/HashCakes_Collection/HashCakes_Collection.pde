/*
 * HASH_MAP_01
 * Exploring Hash Maps 
 * - Very fast for storing & retrieving. Good for IDs
 * - Not for iterating nor sorting - Lists are for that
 *
 * A Map is a set of associations between a pair of objects
 *
 * MW_3.06.16
 * links / refs ...
 * A HashMap stores a collection of objects, each referenced 
 * by a key. This is similar to an Array, only instead of 
 * accessing elements with a numeric index, a String is used.
 * https://docs.oracle.com/javase/tutorial/collections/interfaces/index.html
 *
 * end
 
 */

/////////////////////////// GLOBALS ////////////////////////////


Hash MSG;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  background(0);
  MSG = new Hash();
  
  MSG.addMessage("ID_Draw", "Starting to draw");
  MSG.addMessage("ID_InitMachine", "Setting up machine");
  
  MSG.addMessage("ID_Finished", "Finished drawing");
  
  
  MSG.printOutHashEntries();
  //String m = MSG.getMessage("ID_Finished");
  //println(m);
  
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
}

/////////////////////////// FUNCTIONS ////////////////////////////