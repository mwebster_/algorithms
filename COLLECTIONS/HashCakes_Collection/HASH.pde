import java.util.Map;

/*
 * Nice general purpose class for collecting 
 * & retrieving Strings by ID
 * NOTE : Each message must by given a separate ID !
 *
 */

public class Hash {

  HashMap<String, String> HASH_COLLECTION;

  public Hash() {
    HASH_COLLECTION = new HashMap<String, String>();
  }

  /*
   * Method for adding messages with an ID/Key
   */
  public void addMessage(String _id, String _msg) {
    HASH_COLLECTION.put(_id, _msg);
  }

  /*
   * Method for getting a message from Collection by id/key
   * @param  id  : index of String to return in Collection
   */
  public String getMessage(String _id) {
    String s = HASH_COLLECTION.get(_id);
    return s;
  }


  /*
   * Method for displaying last Hash entry in console
   */
  public void printOutHashEntries() {
    println("······················· HASH ·······················");
    for (Map.Entry em : HASH_COLLECTION.entrySet () ) {
      println("ID: "+em.getKey()+" | Message: "+em.getValue());
    }
    println("···················································· HASH END");
  }
}

