/**
 * Algorithms
 * https://bitbucket.org/mwebster_/algorithms
 * Searching Basics
 * Sketch : Template Search : 
 * Uses Selection Sort & Binary Search
 
 */


void setup() {

  String[] txt = loadStrings("Facebook.txt");
  String allWords = join(txt, " ").toLowerCase(); 
  String[] s = splitTokens(allWords, " ");

  Selection.sortArray(s);
  //Selection.show(s);

  Binary.search(s, "novelty", 0, s.length);
  Comparable c = Binary.getValue();
}


void draw() {
}


public static class Binary {
  static Comparable c = null; 
  static int index;

  public static int find(Comparable[] a, Comparable b, int from, int to) {
    if (from > to) // prevents from index overtaking to index ;—)
      return -1;
    int mid = from +(to-from)/2;

    // if mid is equal to the value, we return the value at mid
    if (b.compareTo(a[mid])==0) {
      return mid;
    }
    // if less than, search again but in the first part of array
    if (b.compareTo(a[mid])<0) { 
      return find(a, b, from, mid-1);
    } else {
      // otherwise search in second half !
      return find(a, b, mid+1, to);
    }
  }

  /**
   * returns the Comparable value
   */

  public static Comparable search(Comparable[] a, Comparable b, int from, int to) {
    c = null;

    if (from > to) // prevents from index overtaking to index ;—)
      return -1;
    int mid = from +(to-from)/2;

    // if mid is equal to the value, we return the value at mid
    if (b.compareTo(a[mid])==0) {
      index = mid;
      return c = a[mid];
    }
    // if less than, search again but in the first part of array
    if (b.compareTo(a[mid])<0) { 
      return search(a, b, from, mid-1);
    } else {
      // otherwise search in second half !
      return search(a, b, mid+1, to);
    }
  }


  public static Comparable getValue() {
    println("We found the value: "+c+ " at position "+index );
    return c;
  }
}