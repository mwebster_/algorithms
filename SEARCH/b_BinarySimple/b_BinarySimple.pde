/**
 * Algorithms
 * https://bitbucket.org/mwebster_/algorithms
 * Searching Basics
 * Sketch : Binary Search :
 * Uses Java Sort & Binary Search on integers
 */




import java.util.*;

void setup() {
  int[] randNum = new int[100];
  for (int i=0; i<randNum.length; i++) {
    int r = (int)random(100);
    randNum[i] = r;
  }
  Arrays.sort(randNum);
  println(randNum);

  if (BinarySearch.rank(6, randNum)>0) {
    println("Found at key index: " + BinarySearch.rank(6, randNum));
  }else {
   println("not found");
  }
}


// Static class for searching an integer in a sorted array list
public static class BinarySearch {
  public static int rank(int key, int[] a) {  // Array must be sorted.
    int lo  = 0;
    int hi = a.length - 1;
    while (lo <= hi)
    {  // Key is in a[lo..hi] or not present.
      int mid = lo + (hi - lo) / 2;
      if      (key < a[mid]) hi = mid - 1;
      else if (key > a[mid]) lo = mid + 1;
      else                   return mid;
    }
    return -1;
  }
}
