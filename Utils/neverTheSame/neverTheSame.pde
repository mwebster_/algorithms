/**
 * Algorithms
 * https://bitbucket.org/mwebster_/algorithms
 * Utils
 * Sketch : NeverTheSame 
 * Generates an array of random numbers that are never the same
 
 */


/////////////////////////// GLOBALS //////////////////////////
int[] randomNumbers;
int num = 9;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(50, 50);
  background(255);
  smooth();
  randomNumbers = new int[num];
  int index = 0;      
  while (index<randomNumbers.length) {
    int n = (int)random(num+1);
    if (checkRandom(n, randomNumbers)) {
      randomNumbers[index] = n;
      index++;
    }
  }

  for (int i = 0; i < randomNumbers.length; ++i) {
    println(randomNumbers[i]);
  }
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
}

/////////////////////////// FUNCTIONS ///////////////////////
/**
 * Method for checking same int numbers
 * @param   n   the int number to check
 * @param   array the array to check
 * @return        returns true or false
 */
boolean checkRandom(int n, int[] array) {
  for (int i=0; i<array.length; i++) {
    if (n == array[i]) return false;
    if (n == 0) return false; // never choose zero (could be handy !)
  }
  return true;
}

void keyPressed() {
  setup();
}