import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class neverTheSame extends PApplet {

    /*
    :::::::::::::::::::::::::::::::::
    RANDOM
    :::::::::::::::::::::::::::::::::

    Sketch : NeverTheSame_01
    
    EXERCISES IN RANDOM CHOICE

     */

    /////////////////////////// GLOBALS //////////////////////////
    int[] RAND_NUMS;
    /////////////////////////// SETUP ////////////////////////////

    public void setup() {
      size(50, 50);
      background(255);
      smooth();
      RAND_NUMS = new int[5];
      int index = 0;      
      while(index<RAND_NUMS.length){
        int num = (int)random(10);
          if(checkRandom(num)) {
            RAND_NUMS[index] = num;
            index++;
          }

      }
      println("First =" +RAND_NUMS[0]);
       println("Second =" +RAND_NUMS[1]);
        println("Third =" +RAND_NUMS[2]);
         println("Fourth =" +RAND_NUMS[3]);
         /*
         for (int i = 0; i < RAND_NUMS.length; ++i) {
           println(RAND_NUMS[i]);
         }
         */
    }

    /////////////////////////// DRAW ////////////////////////////
    public void draw() {
      background(0); 

      

    }

    /////////////////////////// FUNCTIONS ///////////////////////
    /**
   * Method for checking same int numbers
   * @param   num   the int number to check
   * @return        returns true or false
   */
    public boolean checkRandom(int num){
      for(int i=0; i<RAND_NUMS.length; i++){
      if(num == RAND_NUMS[i]) return false;
      if(num == 0) return false; // never choose zero (could be handy !)
    }
      return true;

    }

    public void keyPressed() {
      setup();
    }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--full-screen", "--bgcolor=#030303", "--hide-stop", "neverTheSame" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
