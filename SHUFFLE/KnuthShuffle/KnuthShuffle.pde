/*
 * Knuth Shuffle / Fisher Yates Shuffle
 * REF : https://www.youtube.com/watch?v=PTAzirdy59E
 *
 */


void setup() {
  Integer[] f = { 1, 3, 5, 8, 9, 10, 15, 18, 27, 45, 173};
  shuffle(f);
  println(f);
}

/////////////////////////// FUNCTIONS ////////////////////////////
/**
 * The idea here is we increment the array, generating a random
 * position at each step. As we increment, we swap the current element
 * in the array with the element that is at the randomly chosen position.
 *
 */

import java.util.Random;


void shuffle(Object[] a) {
  int N = a.length;
  Random rand = new Random();
  for (int i=0; i<N; i++) {
    int r = rand.nextInt(i+1); // random element
    swap(a, i, r); // swap current element with the randomly chosen one
  }
}

void swap(Object[] a, int i, int j) {
  Object t = a[i];
  a[i] = a[j];
  a[j] = t;
}
