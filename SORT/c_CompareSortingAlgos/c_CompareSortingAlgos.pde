/**
 * Algorithms
 * http://algs4.cs.princeton.edu/home/
 * Sorting Basics
 * Sketch : Comparing Sort Algorithms
 * A performance benchmark tool for timing various sorting algorithms
 * Pg 256 Algorithms Sedgewick
 * https://visualgo.net/en
 */

ArrayList times;
SortCompare SC;

void setup() {
  SC = new SortCompare();
  times = new ArrayList();

  String alg1 = "Selection";
  String alg2 = "Insertion";
  String alg3 = "Shell";
  String alg4 = "Merge";
  String alg5 = "Quick";
  String alg6 = "Java";

  int N = 5000; // number of random values to sort
  int T = 25; // number of tests to perform (the greater gives more reliable results)

  // Calculate computing time for each algorithm
  double t1 = SC.timeRandomInput(alg1, N, T);
  double t2 = SC.timeRandomInput(alg2, N, T);
  double t3 = SC.timeRandomInput(alg3, N, T);
  double t4 = SC.timeRandomInput(alg4, N, T);
  double t5 = SC.timeRandomInput(alg5, N, T);
  double t6 = SC.timeRandomInput(alg6, N, T);

  // Results:
  println("Selection: "+ t1);
  println("Insertion: "+ t2);
  println("Shell: "+ t3);
  println("Merge: "+ t4);
  println("Quick: "+ t5);
  println("Java: "+ t6);

  times.add(t1);
  times.add(t2);
  times.add(t3);
  times.add(t4);
  times.add(t5);
  times.add(t6);


  double fastest = findFastest(times);
  println("Fastest time = " + fastest );

  double slowest = findSlowest(times);
  double p = slowest/fastest;
  println("It is " +p + " faster");
}


void draw() {
}
