import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class ShellSort extends PApplet {

/**
 * Algorithms
 * http://algs4.cs.princeton.edu/home/
 * Sorting Basics
 * Sketch : Shell Sort
 * Pg 265 Algorithms Sedgewick
 * https://www.youtube.com/watch?v=E6rBA_YyftI
 */

//NOTE: Shell Sort is much faster than Insertion Sort

public void setup() {

  String[] a = {"Mark", "Webster", "John", "Anthony", "Jeff"};
  Shell.sort(a);
  Shell.show(a);
  println( Shell.isSorted(a) );

  Float[] randNums = { 2.2f, 3.1f, 100.067f, 78.00f, 0.003f, 37f};
  Shell.sort(randNums);
  Shell.show(randNums);
}


public void draw() {
}


public static class Shell  {
  
  public static void sort(Comparable[] a) {
    int N = a.length;
    int h = 1;
    // sort a[i] into increasing order.
    while(h < N/3) h = 3*h + 1; // 1, 4, 13, 40 ...
     while(h >=1) {
      for (int i = h; i < N; i++) {
        for(int j=i; j >= h && less(a[j], a [j-h]); j-=h)
        swap(a, j, j-h);
    }
    h = h/3;
  }
}

  /**
   * returns true when the item v is less than item w
   */
  private static boolean less(Comparable v, Comparable w) {
    return v.compareTo(w) < 0; // (-1 v<w || 0 v=w || 1 v>w : These are the values from compareTo)
  }

  /**
   * Swaps two elements in an array
   * @param   a      the array with the two elements to swap
   * @param   i      index of one of the elements
   * @param   j      index of the other element
   */
  private static void swap(Comparable[] a, int i, int j) {
    Comparable t = a[i];
    a[i] = a[j];
    a[j] = t;
  }

  private static void show(Comparable[] a) {
    for (int i=0; i<a.length; i++)
      println(a[i] + " ");
  }

  public static boolean isSorted(Comparable[] a) {
    println("Number of items sorted: "+a.length);
    for (int i=1; i<a.length; i++)
      if (less(a[i], a[i-1]))
        return false;
    return true;
  }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#000000", "--hide-stop", "ShellSort" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
