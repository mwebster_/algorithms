# Sorting Algorithms


## Introduction

To sort an array in ascending order there exists a multitude of algorithms each with their own criteria depending on the array to sort. The basic structure for these algorithms remain very similar in that they all share a number of common methods. The basic principle is to search for a value that is *"less than"* something and *swap* this in accordance. Each algorithm implements a sort method with more sophisticated ones using a partioning method for dealing with various parts of the array. 

The performance of these algorithms depends on the sort method and the number of check with swap operations. i.e. how often we need to check for two values and how often we must swap these. These two operations have direct effect on the algorithms' performance. We can test for these using a simple timer class. See the CompareSortingAlgos program for an example of this. 

## Contents

- **Insertion**
- **Merge**
- **Quick**
- **Shell**

- **TemplateSort**

    Simple template sketch. Uses Selection Sort.
    
- **FactorySort**

    Implements Factory Method design pattern for using different sorting algorithms.
    
- **CompareSortingAlgos**

    Measures & compares performance of various sorting algorithms on floating point numbers.
    
- **CompareSortingAlgos_text**

    Measures & compares performance of various sorting algorithms on a short text.
    