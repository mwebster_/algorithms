import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.Random; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class b_FactorySortPattern extends PApplet {

/*
 * ::::::::::::::::::::::::::::
 * DESIGN PATTERNS & STRATEGIES
 * ::::::::::::::::::::::::::::
 *
 * Sketch : FactorySortPattern
 *
 * Summary : A factory of object instances for applying sort algorithms
 * author : mw2018
 * web : https://bitbucket.org/mwebster_/algorithms
 */




SortFactory SF;

public void setup() {

  // input data
  String[] txt = loadStrings("Facebook.txt");
  String allWords = join(txt, " ").toLowerCase();
  String[] s = splitTokens(allWords, " ");
  Double[] randNum = generateRand(100);

  // chosen sort algorithms
  SF = new SortFactory();
  Sort algo1 = SF.chooseSort("SHELL");
  Sort algo2 = SF.chooseSort("QUICK");

  // computation
  Timer timer = new Timer();
  algo1.sort(s);
  algo1.show(s);



  algo2.sort(randNum);
  algo2.show(randNum);

  println(" ");
  println(":::: TIMER >>>");
  println("Calculation time: "+timer.elapsedTime());
}


public void draw() {
}


///////////////// FUNCTIONS

public Double[] generateRand(int N) {
  Random rand = new Random();
  Double[] r = new Double[N];
  for (int i=0; i<N; i++) {
    r[i] = rand.nextDouble();
  }
  return r;
}
/**
 * Factory Strategy for sorting algorithms
 */



public interface Sort {

  public void sort(Comparable[] a);
  public void show(Comparable[] a);
}


public class SortFactory {

  public Sort chooseSort(String sortAlgo) {
    if (sortAlgo == null) {
      return null;
    }
    if (sortAlgo.equalsIgnoreCase("QUICK")) {
      println("Chosen algorithm: Quick Sort");
      return new Quick();
    }

    if (sortAlgo.equalsIgnoreCase("SELECTION")) {
      println("Chosen algorithm: Selection Sort");
      return new Selection();
    }
    
    if (sortAlgo.equalsIgnoreCase("SHELL")) {
      println("Chosen algorithm: Shell Sort");
      return new Shell();
    }
    
    println("Error: String not recognised");
    return null;
  }
}
/**
 * Quick Sort
 */


public class Quick implements Sort {   
  public void sort(Comparable[] a) {
    shuffle(a);
    sort(a, 0, a.length-1);
  }

  private void sort(Comparable[] a, int low, int hi) {
    if (hi <= low) return;
    int j = partition(a, low, hi);
    sort(a, low, j-1);
    sort(a, j+1, hi);
  }

  private int partition(Comparable[] a, int low, int hi) {

    int i = low, j = hi+1;
    Comparable v = a[low];
    while (true) {
      while (less(a[++i], v)) if (i == hi) break;
      while (less(v, a[--j])) if (i == low) break;
      if (i >= j) break;
      swap(a, i, j);
    }
    swap(a, low, j);
    return j;
  }

  /**
   * Swaps two elements in an array
   * @param   a      the array with the two elements to swap
   * @param   i      index of one of the elements
   * @param   j      index of the other element
   */
  private void swap(Comparable[] a, int i, int j) {
    Comparable t = a[i]; 
    a[i] = a[j]; 
    a[j] = t;
  }

  private void shuffle(Comparable[] a) {
    int N = a.length;
    Random rand = new Random();
    for (int i=0; i<N; i++) {
      int r = rand.nextInt(i+1); // random element
      swap(a, i, r); // swap current element with the randomly chosen one
    }
  }

  /**
   * returns true when the item v is less than item w
   */
  private boolean less(Comparable v, Comparable w) {
    return v.compareTo(w) < 0; // (-1 v<w || 0 v=w || 1 v>w : These are the values from compareTo)
  }

  public void show(Comparable[] a) {
    for (int i=0; i<a.length; i++) 
      println(a[i] + " ");
  }

  public boolean isSorted(Comparable[] a) {
    println("Number of items sorted: "+a.length);
    for (int i=1; i<a.length; i++)
      if (less(a[i], a[i-1])) 
        return false;
    return true;
  }
}
/**
 * Selection Sort
 */


public class Selection implements Sort{

  public void sort(Comparable[] a) {
    int N = a.length;               // array length
    // Exchange a[i] with smallest entry in a[i+1...N).
    for (int i = 0; i < N; i++) {  
      int min = i;                 // index of minimal entr.
      for (int j = i+1; j < N; j++)
        if (less(a[j], a[min])) min = j;
      swap(a, i, min);
    }
  }

  private boolean less(Comparable v, Comparable w) {
    return v.compareTo(w) < 0;
  }

  private void swap(Comparable[] a, int i, int j) {
    Comparable t = a[i]; 
    a[i] = a[j]; 
    a[j] = t;
  }

  public void show(Comparable[] a) {
    for (int i=0; i<a.length; i++) 
      println(a[i] + " ");
  }

  public boolean isSorted(Comparable[] a) {
    println("Number of items sorted: "+a.length);
    for (int i=1; i<a.length; i++)
      if (less(a[i], a[i-1])) 
        return false;
    return true;
  }
}
/**
 * Shell Sort
 */

public class Shell implements Sort  {
  
  public void sort(Comparable[] a) {
    int N = a.length;  
    int h = 1;
    // sort a[i] into increasing order.
    while(h < N/3) h = 3*h + 1; // 1, 4, 13, 40 ...
     while(h >=1) {
      for (int i = h; i < N; i++) {
        for(int j=i; j >= h && less(a[j], a [j-h]); j-=h)
        swap(a, j, j-h);
    }
    h = h/3;
  }
}

  /**
   * returns true when the item v is less than item w
   */
  private boolean less(Comparable v, Comparable w) {
    return v.compareTo(w) < 0; // (-1 v<w || 0 v=w || 1 v>w : These are the values from compareTo)
  }

  /**
   * Swaps two elements in an array
   * @param   a      the array with the two elements to swap
   * @param   i      index of one of the elements
   * @param   j      index of the other element
   */
  private void swap(Comparable[] a, int i, int j) {
    Comparable t = a[i]; 
    a[i] = a[j]; 
    a[j] = t;
  }

  public void show(Comparable[] a) {
    for (int i=0; i<a.length; i++) 
      println(a[i] + " ");
  }

  public boolean isSorted(Comparable[] a) {
    println("Number of items sorted: "+a.length);
    for (int i=1; i<a.length; i++)
      if (less(a[i], a[i-1])) 
        return false;
    return true;
  }
}
/**
 * Timer - used for measuring computation time
 */

public class Timer {
  private final long start;

  /**
   * Initializes a new stopwatch.
   */
  public Timer() {
    start = System.currentTimeMillis();
  } 


  /**
   * Returns the elapsed CPU time (in seconds) since the stopwatch was created.
   *
   * @return elapsed CPU time (in seconds) since the stopwatch was created
   */
  public double elapsedTime() {
    long now = System.currentTimeMillis();
    return (now - start) / 1000.0f;
  }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#000000", "--hide-stop", "b_FactorySortPattern" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
