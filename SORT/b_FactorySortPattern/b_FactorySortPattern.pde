/*
 * ::::::::::::::::::::::::::::
 * DESIGN PATTERNS & STRATEGIES
 * ::::::::::::::::::::::::::::
 *
 * Sketch : FactorySortPattern
 *
 * Summary : A factory of object instances for applying sort algorithms
 * Author : mw2018
 * Web : https://bitbucket.org/mwebster_/algorithms
 */


import java.util.Random;

SortFactory SF;

void setup() {

  // input data
  String[] txt = loadStrings("Facebook.txt");
  String allWords = join(txt, " ").toLowerCase();
  String[] s = splitTokens(allWords, " ");
  Double[] randNum = generateRand(100);

  // chosen sort algorithms
  SF = new SortFactory();
  Sort algo1 = SF.chooseSort("SHELL");
  Sort algo2 = SF.chooseSort("QUICK");

  // computation
  Timer timer = new Timer();
  // text
  algo1.sort(s);
  algo1.show(s);

  // floating point numbers
  algo2.sort(randNum);
  algo2.show(randNum);

  println(" ");
  println(":::: TIMER >>>");
  println("Calculation time: "+timer.elapsedTime());
}


void draw() {
}


///////////////// FUNCTIONS

Double[] generateRand(int N) {
  Random rand = new Random();
  Double[] r = new Double[N];
  for (int i=0; i<N; i++) {
    r[i] = rand.nextDouble();
  }
  return r;
}
