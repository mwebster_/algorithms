/**
 * Algorithms
 * http://algs4.cs.princeton.edu/home/
 * Sorting Basics
 * Sketch : Insertion Sort
 * Pg 265 Algorithms Sedgewick
 * https://www.youtube.com/watch?v=alh3Jme9LZs
 */

//NOTE: Insertion Sort is excellent for short and partially sorted arrays

void setup() {
  
  // input data  
  String[] a = {"Mark", "Webster", "John", "Anthony", "Jeff"};
  Float[] randNums = { 2.2f, 3.1, 100.067, 78.00, 0.003, 37f};
  
  // computation
  Timer timer = new Timer();
  Insertion.sort(a);
  Insertion.show(a);
  println( Insertion.isSorted(a) );
  
  
  Insertion.sort(randNums);
  Insertion.show(randNums);
  
  println(" ");
  println(":::: TIMER >>>");
  println("Calculation time: "+timer.elapsedTime());
}


void draw() {
}


public static class Insertion  {
  
  public static void sort(Comparable[] a) {
    int N = a.length;    
    // sort a[i] into increasing order.
    for (int i = 1; i < N; i++) {  
      for (int j = i; j > 0 && less(a[j], a[j-1]); j--)
        swap(a, j, j-1);
    }
  }

  /**
   * returns true when the item v is less than item w
   */
  private static boolean less(Comparable v, Comparable w) {
    return v.compareTo(w) < 0; // (-1 v<w || 0 v=w || 1 v>w : These are the values from compareTo)
  }

  /**
   * Swaps two elements in an array
   * @param   a      the array with the two elements to swap
   * @param   i      index of one of the elements
   * @param   j      index of the other element
   */
  private static void swap(Comparable[] a, int i, int j) {
    Comparable t = a[i]; 
    a[i] = a[j]; 
    a[j] = t;
  }

  private static void show(Comparable[] a) {
    for (int i=0; i<a.length; i++) 
      println(a[i] + " ");
  }

  public static boolean isSorted(Comparable[] a) {
    println("Number of items sorted: "+a.length);
    for (int i=1; i<a.length; i++)
      if (less(a[i], a[i-1])) 
        return false;
    return true;
  }
}