/**
 * Algorithms
 * http://algs4.cs.princeton.edu/home/
 * Sorting Basics
 * Sketch : Template Sort : uses Selection Sort
 * Pg 259 Algorithms Sedgewick
 * https://www.youtube.com/watch?v=cJF9vjz89vU
 * https://visualgo.net/en
 */


// NOTE: Static class but we can only sort data types that implement
// Comparable - Integer, Float, String for example.
// NO primitive data types.

void setup() {

  String[] a = {"Mark", "Webster", "John", "Anthony", "Jeff"};
  Example.sortArray(a);
  Example.show(a);
  println( Example.isSorted(a) );

  Float[] randNums = { 2.2f, 3.1, 100.067, 78.00, 0.003, 37f};
  Example.sortArray(randNums);
  Example.show(randNums);
}


void draw() {
}


public static class Example  {

  public static void sortArray(Comparable[] a) {
    int N = a.length;               // array length
    for (int i = 0; i < N; i++) {
      // Exchange a[i] with smallest entry in a[i+1...N).
      int min = i;                 // index of minimal entr.
      for (int j = i+1; j < N; j++)
        // we check to see if the next item is less than the last one
        // if it is then we modifiy the min index value (move up in the array)
        // and swap the position in the array of the lesser item with the greater.
        //
        if (less(a[j], a[min])) min = j;
        swap(a, i, min);
    }
  }

  /**
   * returns true when the item v is less than item w
   */
  private static boolean less(Comparable v, Comparable w) {
    return v.compareTo(w) < 0; // (-1 v<w || 0 v=w || 1 v>w : These are the values from compareTo)
  }

/*
  @Override
    int compareTo(String s) {
    return 0;
  }
*/

  /**
   * Swaps two elements in an array
   * @param   a      the array with the two elements to swap
   * @param   i      index of one of the elements
   * @param   j      index of the other element
   */
  private static void swap(Comparable[] a, int i, int j) {
    Comparable t = a[i];
    a[i] = a[j];
    a[j] = t;
  }

  private static void show(Comparable[] a) {
    for (int i=0; i<a.length; i++)
      println(a[i] + " ");
  }

  public static boolean isSorted(Comparable[] a) {
    println("Number of items sorted: "+a.length);
    for (int i=1; i<a.length; i++)
      if (less(a[i], a[i-1]))
        return false;
    return true;
  }
}
