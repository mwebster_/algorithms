/**
 * Algorithms
 * http://algs4.cs.princeton.edu/home/
 * Sorting Basics
 * Sketch : Merge Sort
 * Pg 270 Algorithms Sedgewick
 */

//NOTE: Merge Sort

void setup() {

  String[] a = {"Mark", "Webster", "John", "Anthony", "Jeff"};
  Merge.sort(a);
  Merge.show(a);
  println( Merge.isSorted(a) );

  Float[] randNums = { 2.2f, 3.1, 100.067, 78.00, 0.003, 37f};
  Merge.sort(randNums);
  Merge.show(randNums);
}


void draw() {
}


public static class Merge  {

  private static Comparable[] aux;
  public static void sort(Comparable[] a) {
   aux = new Comparable[a.length];
   sort(a, 0, a.length-1);
}

private static void sort(Comparable[] a, int low, int hi){
  if(hi <= low) return;
  int mid = low + (hi - low)/2;
  sort(a, low, mid);
  sort(a, mid+1, hi);
  merge(a, low, mid, hi);

}

/**
 * This merge method copies into an aux array
 * & then
 */
public static void merge(Comparable[] a, int low, int mid, int hi){
  int i = low, j = mid+1;
  for(int k=low; k <= hi; k++)
    aux[k] = a[k]; // make a copy first of array

    for(int k = low; k <= hi; k++) // merge back to a[low...hi].
      if(i > mid)                   a[k] = aux[j++]; // take from right half
      else if(j > hi)               a[k] = aux[i++]; // take from left half
      else if(less(aux[j], aux[i])) a[k] = aux[j++]; // if current key from right is less than on left, take from right
      else                          a[k] = aux[i++]; // else take from left
}

  /**
   * returns true when the item v is less than item w
   */
  private static boolean less(Comparable v, Comparable w) {
    return v.compareTo(w) < 0; // (-1 v<w || 0 v=w || 1 v>w : These are the values from compareTo)
  }

  /**
   * Swaps two elements in an array
   * @param   a      the array with the two elements to swap
   * @param   i      index of one of the elements
   * @param   j      index of the other element
   */
  private static void swap(Comparable[] a, int i, int j) {
    Comparable t = a[i];
    a[i] = a[j];
    a[j] = t;
  }

  private static void show(Comparable[] a) {
    for (int i=0; i<a.length; i++)
      println(a[i] + " ");
  }

  public static boolean isSorted(Comparable[] a) {
    println("Number of items sorted: "+a.length);
    for (int i=1; i<a.length; i++)
      if (less(a[i], a[i-1]))
        return false;
    return true;
  }
}
