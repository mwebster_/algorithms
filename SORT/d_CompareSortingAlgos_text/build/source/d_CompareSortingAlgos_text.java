import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.Random; 
import java.util.Arrays; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class d_CompareSortingAlgos_text extends PApplet {

/**
 * Algorithms
 * http://algs4.cs.princeton.edu/home/
 * Sorting Basics
 * Sketch : Comparing Sort Algorithms with text input
 * A performance benchmark tool for timing various sorting algorithms
 * Pg 256 Algorithms Sedgewick
 * https://visualgo.net/en
 */

ArrayList times;
SortCompare SC;

public void setup() {
  // load text first
  String[] txt = loadStrings("Facebook.txt");
  String allWords = join(txt, " ").toLowerCase();
  String[] s = splitTokens(allWords, " ");

  SC = new SortCompare();
  times = new ArrayList();

  String alg1 = "Selection";
  String alg2 = "Insertion";
  String alg3 = "Shell";
  String alg4 = "Merge";
  String alg5 = "Quick";
  String alg6 = "Java";

  //int N = 5000; // number of random values
  int T = 1; // number of tests to perform

  double t1 = SC.timeTextInput(alg1, s, T);
  double t2 = SC.timeTextInput(alg2, s, T);
  double t3 = SC.timeTextInput(alg3, s, T);
  double t4 = SC.timeTextInput(alg4, s, T);
  double t5 = SC.timeTextInput(alg5, s, T);
  double t6 = SC.timeTextInput(alg6, s, T);


  // Results:

  println("Selection: "+ t1);
  println("Insertion: "+ t2);
  println("Shell: "+ t3);
  println("Merge: "+ t4);
  println("Quick: "+ t5);
  println("Java: "+ t6);

  times.add(t1);
  times.add(t2);
  times.add(t3);
  times.add(t4);
  times.add(t5);
  times.add(t6);


  double fastest = findFastest(times);
  println("Fastest time = " + fastest );

  double slowest = findSlowest(times);
  double p = slowest/fastest;
  println("It is " +p + " faster");


}


public void draw() {}
/**
 * A class for timing different sort algorithms
 */




public class SortCompare {

  SortCompare() {
  }
  
  public double time(String alg, Double[] a) {
    Timer timer = new Timer();
    if (alg.equals("Selection")) Selection.sort(a);
    if(alg.equals("Insertion")) Insertion.sort(a);
    if(alg.equals("Shell")) Shell.sort(a);
    if(alg.equals("Merge")) Merge.sort(a);
    if(alg.equals("Quick")) Quick.sort(a);
    if(alg.equals("Java")) Arrays.sort(a); // Java Arrays sort method
    return timer.elapsedTime();
  }
  
    public double timeText(String alg, String[] a) {
    Timer timer = new Timer();
    if (alg.equals("Selection")) Selection.sort(a);
    if(alg.equals("Insertion")) Insertion.sort(a);
    if(alg.equals("Shell")) Shell.sort(a);
    if(alg.equals("Merge")) Merge.sort(a);
    if(alg.equals("Quick")) Quick.sort(a);
    if(alg.equals("Java")) Arrays.sort(a); // Java Arrays sort method
    return timer.elapsedTime();
  }

  /**
   * Performs benchmark timer for sorting a uniform random set of values
   * String alg : the algorithm type
   * int N : number of random values to generate & sort
   * int T : number of tests to perform
   */
  public double timeRandomInput(String alg, int N, int T) {

    double total = 0.0f;
    Double[]a = new Double[N];
    Random rand = new Random();

    for (int t=0; t < T; t++) {

      for (int i=0; i<N; i++) 
        a[i] = rand.nextDouble(); 
        total += time(alg, a);
    }
    return total;
  }
  
    /**
   * Performs benchmark timer for sorting text
   * String alg : the algorithm type
   * String s : the String to sort
   * int T : number of tests to perform
   */
  public double timeTextInput(String alg, String[] s, int T) {

    double total = 0.0f;
    int N = s.length;
    String[]a = new String[N];
    a = s; // assign
    
    for (int t=0; t < T; t++) {

      for (int i=0; i<N; i++) 
       total += timeText(alg, a);
    }
    return total;
  }
}
/**
 * Insertion Sort
 */
 
 

public static class Insertion  {
  
  public static void sort(Comparable[] a) {
    int N = a.length;    
    // sort a[i] into increasing order.
    for (int i = 1; i < N; i++) {  
      for (int j = i; j > 0 && less(a[j], a[j-1]); j--)
        swap(a, j, j-1);
    }
  }

  /**
   * returns true when the item v is less than item w
   */
  private static boolean less(Comparable v, Comparable w) {
    return v.compareTo(w) < 0; // (-1 v<w || 0 v=w || 1 v>w : These are the values from compareTo)
  }

  /**
   * Swaps two elements in an array
   * @param   a      the array with the two elements to swap
   * @param   i      index of one of the elements
   * @param   j      index of the other element
   */
  private static void swap(Comparable[] a, int i, int j) {
    Comparable t = a[i]; 
    a[i] = a[j]; 
    a[j] = t;
  }

  private static void show(Comparable[] a) {
    for (int i=0; i<a.length; i++) 
      println(a[i] + " ");
  }

  public static boolean isSorted(Comparable[] a) {
    println("Number of items sorted: "+a.length);
    for (int i=1; i<a.length; i++)
      if (less(a[i], a[i-1])) 
        return false;
    return true;
  }
}
/**
 * Merge Sort
 */
 
 
 public static class Merge  {
  
  private static Comparable[] aux;
  public static void sort(Comparable[] a) {
   aux = new Comparable[a.length];
   sort(a, 0, a.length-1);
}

private static void sort(Comparable[] a, int low, int hi){
  if(hi <= low) return;
  int mid = low + (hi - low)/2;
  sort(a, low, mid);
  sort(a, mid+1, hi);
  merge(a, low, mid, hi);
  
}

/**
 * This merge method copies into an aux array
 * & then 
 */
public static void merge(Comparable[] a, int low, int mid, int hi){
  int i = low, j = mid+1;
  for(int k=low; k <= hi; k++)
    aux[k] = a[k]; // make a copy first of array
    
    for(int k = low; k <= hi; k++) // merge back to a[low...hi].
      if(i > mid)                  a[k] = aux[j++]; // take from right half
      else if(j > hi)              a[k] = aux[i++]; // take from left half
      else if(less(aux[j], aux[i])) a[k] = aux[j++]; // if current key from right is less than on left, take from right
      else                         a[k] = aux[i++]; // else take from left
}

  /**
   * returns true when the item v is less than item w
   */
  private static boolean less(Comparable v, Comparable w) {
    return v.compareTo(w) < 0; // (-1 v<w || 0 v=w || 1 v>w : These are the values from compareTo)
  }

  private static void show(Comparable[] a) {
    for (int i=0; i<a.length; i++) 
      println(a[i] + " ");
  }

  public static boolean isSorted(Comparable[] a) {
    println("Number of items sorted: "+a.length);
    for (int i=1; i<a.length; i++)
      if (less(a[i], a[i-1])) 
        return false;
    return true;
  }
}
/**
 * Quick Sort
 */
 
 
 public static class Quick {
  public static void sort(Comparable[] a) {
    shuffle(a);
    sort(a, 0, a.length-1);
  }

  private static void sort(Comparable[] a, int low, int hi) {
    if (hi <= low) return;
    int j = partition(a, low, hi);
    sort(a, low, j-1);
    sort(a, j+1, hi);
  }

  private static int partition(Comparable[] a, int low, int hi){
    
   int i = low, j = hi+1;
   Comparable v = a[low];
   while(true) {
    while(less(a[++i], v)) if (i == hi) break;
    while(less(v, a[--j])) if (i == low) break;
    if(i >= j) break;
    swap(a, i, j);     
   }
   swap(a, low, j);
   return j;
    
  }
  
   /**
   * Swaps two elements in an array
   * @param   a      the array with the two elements to swap
   * @param   i      index of one of the elements
   * @param   j      index of the other element
   */
  private static void swap(Comparable[] a, int i, int j) {
    Comparable t = a[i]; 
    a[i] = a[j]; 
    a[j] = t;
  }
  
  private static void shuffle(Comparable[] a) {
  int N = a.length;
  Random rand = new Random();
  for (int i=0; i<N; i++) {
    int r = rand.nextInt(i+1); // random element
    swap(a, i, r); // swap current element with the randomly chosen one
  }
}

  /**
   * returns true when the item v is less than item w
   */
  private static boolean less(Comparable v, Comparable w) {
    return v.compareTo(w) < 0; // (-1 v<w || 0 v=w || 1 v>w : These are the values from compareTo)
  }

  private static void show(Comparable[] a) {
    for (int i=0; i<a.length; i++) 
      println(a[i] + " ");
  }

  public static boolean isSorted(Comparable[] a) {
    println("Number of items sorted: "+a.length);
    for (int i=1; i<a.length; i++)
      if (less(a[i], a[i-1])) 
        return false;
    return true;
  }
}
/**
 * Selection Sort
 */
 
 
 public static class Selection  {
  
  public static void sort(Comparable[] a) {
    int N = a.length;               // array length
    for (int i = 0; i < N; i++) {  
      // Exchange a[i] with smallest entry in a[i+1...N).
      int min = i;                 // index of minimal entr.
      for (int j = i+1; j < N; j++)
        // we check to see if the next item is less than the last one
        // if it is then we modifiy the min index value (move up in the array)
        // and swap the position in the array of the lesser item with the greater.
        // 
        if (less(a[j], a[min])) min = j;
        swap(a, i, min);
    }
  }

  /**
   * returns true when the item v is less than item w
   */
  private static boolean less(Comparable v, Comparable w) {
    return v.compareTo(w) < 0; // (-1 v<w || 0 v=w || 1 v>w : These are the values from compareTo)
  }

/*
  @Override
    int compareTo(String s) {
    return 0;
  }
*/
  /**
   * Swaps two elements in an array
   * @param   a      the array with the two elements to swap
   * @param   i      index of one of the elements
   * @param   j      index of the other element
   */
  private static void swap(Comparable[] a, int i, int j) {
    Comparable t = a[i]; 
    a[i] = a[j]; 
    a[j] = t;
  }

  private static void show(Comparable[] a) {
    for (int i=0; i<a.length; i++) 
      println(a[i] + " ");
  }

  public static boolean isSorted(Comparable[] a) {
    println("Number of items sorted: "+a.length);
    for (int i=1; i<a.length; i++)
      if (less(a[i], a[i-1])) 
        return false;
    return true;
  }
}
/**
 * Shell Sort
 */

public static class Shell  {
  
  public static void sort(Comparable[] a) {
    int N = a.length;  
    int h = 1;
    // sort a[i] into increasing order.
    while(h < N/3) h = 3*h + 1; // 1, 4, 13, 40 ...
     while(h >=1) {
      for (int i = h; i < N; i++) {
        for(int j=i; j >= h && less(a[j], a [j-h]); j-=h)
        swap(a, j, j-h);
    }
    h = h/3;
  }
}

  /**
   * returns true when the item v is less than item w
   */
  private static boolean less(Comparable v, Comparable w) {
    return v.compareTo(w) < 0; // (-1 v<w || 0 v=w || 1 v>w : These are the values from compareTo)
  }

  /**
   * Swaps two elements in an array
   * @param   a      the array with the two elements to swap
   * @param   i      index of one of the elements
   * @param   j      index of the other element
   */
  private static void swap(Comparable[] a, int i, int j) {
    Comparable t = a[i]; 
    a[i] = a[j]; 
    a[j] = t;
  }

  private static void show(Comparable[] a) {
    for (int i=0; i<a.length; i++) 
      println(a[i] + " ");
  }

  public static boolean isSorted(Comparable[] a) {
    println("Number of items sorted: "+a.length);
    for (int i=1; i<a.length; i++)
      if (less(a[i], a[i-1])) 
        return false;
    return true;
  }
}
public class Timer {
  private final long start;

  /**
   * Initializes a new stopwatch.
   */
  public Timer() {
    start = System.currentTimeMillis();
  } 


  /**
   * Returns the elapsed CPU time (in seconds) since the stopwatch was created.
   *
   * @return elapsed CPU time (in seconds) since the stopwatch was created
   */
  public double elapsedTime() {
    long now = System.currentTimeMillis();
    return (now - start) / 1000.0f;
  }
}
/**
 * Some little functions
 */


public double findFastest(ArrayList<Double> results) {
  double t = 0;
  int i = 1;
  double temp = results.get(0);
  while (i<results.size()) {    
    Double next = results.get(i);
    if (temp<next) {
    } else {
      temp = next;
    }
    i++;
    t = temp;
  }
  return t;
}

public double findSlowest(ArrayList<Double> results) {
  double t = 0;
  int i = 1;
  double temp = results.get(0);
  while (i<results.size()) {    
    Double next = results.get(i);
    if (temp<next) {
      temp = next;
    } else {
      //temp = next;
    }
    i++;
    t = temp;
  }
  return t;
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#000000", "--hide-stop", "d_CompareSortingAlgos_text" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
