/**
 * A class for timing different sort algorithms
 */

import java.util.Random;
import java.util.Arrays;

public class SortCompare {

  SortCompare() {
  }
  
  public double time(String alg, Double[] a) {
    Timer timer = new Timer();
    if (alg.equals("Selection")) Selection.sort(a);
    if(alg.equals("Insertion")) Insertion.sort(a);
    if(alg.equals("Shell")) Shell.sort(a);
    if(alg.equals("Merge")) Merge.sort(a);
    if(alg.equals("Quick")) Quick.sort(a);
    if(alg.equals("Java")) Arrays.sort(a); // Java Arrays sort method
    return timer.elapsedTime();
  }
  
    public double timeText(String alg, String[] a) {
    Timer timer = new Timer();
    if (alg.equals("Selection")) Selection.sort(a);
    if(alg.equals("Insertion")) Insertion.sort(a);
    if(alg.equals("Shell")) Shell.sort(a);
    if(alg.equals("Merge")) Merge.sort(a);
    if(alg.equals("Quick")) Quick.sort(a);
    if(alg.equals("Java")) Arrays.sort(a); // Java Arrays sort method
    return timer.elapsedTime();
  }

  /**
   * Performs benchmark timer for sorting a uniform random set of values
   * String alg : the algorithm type
   * int N : number of random values to generate & sort
   * int T : number of tests to perform
   */
  public double timeRandomInput(String alg, int N, int T) {

    double total = 0.0f;
    Double[]a = new Double[N];
    Random rand = new Random();

    for (int t=0; t < T; t++) {

      for (int i=0; i<N; i++) 
        a[i] = rand.nextDouble(); 
        total += time(alg, a);
    }
    return total;
  }
  
    /**
   * Performs benchmark timer for sorting text
   * String alg : the algorithm type
   * String s : the String to sort
   * int T : number of tests to perform
   */
  public double timeTextInput(String alg, String[] s, int T) {

    double total = 0.0f;
    int N = s.length;
    String[]a = new String[N];
    a = s; // assign
    
    for (int t=0; t < T; t++) {

      for (int i=0; i<N; i++) 
       total += timeText(alg, a);
    }
    return total;
  }
}