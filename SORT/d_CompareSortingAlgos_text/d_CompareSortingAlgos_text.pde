/**
 * Algorithms
 * http://algs4.cs.princeton.edu/home/
 * Sorting Basics
 * Sketch : Comparing Sort Algorithms with text input
 * A performance benchmark tool for timing various sorting algorithms
 * Pg 256 Algorithms Sedgewick
 * https://visualgo.net/en
 */

ArrayList times;
SortCompare SC;

void setup() {
  // load text first
  String[] txt = loadStrings("Facebook.txt");
  String allWords = join(txt, " ").toLowerCase();
  String[] s = splitTokens(allWords, " ");

  SC = new SortCompare();
  times = new ArrayList();

  String alg1 = "Selection";
  String alg2 = "Insertion";
  String alg3 = "Shell";
  String alg4 = "Merge";
  String alg5 = "Quick";
  String alg6 = "Java";

  //int N = 5000; // number of random values
  int T = 1; // number of tests to perform

  double t1 = SC.timeTextInput(alg1, s, T);
  double t2 = SC.timeTextInput(alg2, s, T);
  double t3 = SC.timeTextInput(alg3, s, T);
  double t4 = SC.timeTextInput(alg4, s, T);
  double t5 = SC.timeTextInput(alg5, s, T);
  double t6 = SC.timeTextInput(alg6, s, T);


  // Results:

  println("Selection: "+ t1);
  println("Insertion: "+ t2);
  println("Shell: "+ t3);
  println("Merge: "+ t4);
  println("Quick: "+ t5);
  println("Java: "+ t6);

  times.add(t1);
  times.add(t2);
  times.add(t3);
  times.add(t4);
  times.add(t5);
  times.add(t6);


  double fastest = findFastest(times);
  println("Fastest time = " + fastest );

  double slowest = findSlowest(times);
  double p = slowest/fastest;
  println("It is " +p + " faster");


}


void draw() {}
