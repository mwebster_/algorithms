import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.Random; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class QuickSort extends PApplet {

/**
 * Algorithms
 * http://algs4.cs.princeton.edu/home/
 * Sorting Basics
 * Sketch : Quick Sort
 * Pg 270 Algorithms Sedgewick
 */

//NOTE: Quick Sort is similar to Merge Sort.



public void setup() {

  // input data
  String[] a = {"Mark", "Webster", "John", "Anthony", "Jeff"};
  Float[] randNums = { 2.2f, 3.1f, 100.067f, 78.00f, 0.003f, 37f};

  // computation
  Timer timer = new Timer();
  Quick.sort(a);
  Quick.show(a);
  println( Quick.isSorted(a) );


  Quick.sort(randNums);
  Quick.show(randNums);

  println(" ");
  println(":::: TIMER >>>");
  println("Calculation time: "+timer.elapsedTime());
}


public void draw() {}


public static class Quick {
  public static void sort(Comparable[] a) {
    shuffle(a);
    sort(a, 0, a.length-1);
  }

  private static void sort(Comparable[] a, int low, int hi) {
    if (hi <= low) return;
    int j = partition(a, low, hi);
    sort(a, low, j-1);
    sort(a, j+1, hi);
  }

  private static int partition(Comparable[] a, int low, int hi){

   int i = low, j = hi+1;
   Comparable v = a[low];
   while(true) {
    while(less(a[++i], v)) if (i == hi) break;
    while(less(v, a[--j])) if (i == low) break;
    if(i >= j) break;
    swap(a, i, j);
   }
   swap(a, low, j);
   return j;

  }

   /**
   * Swaps two elements in an array
   * @param   a      the array with the two elements to swap
   * @param   i      index of one of the elements
   * @param   j      index of the other element
   */
  private static void swap(Comparable[] a, int i, int j) {
    Comparable t = a[i];
    a[i] = a[j];
    a[j] = t;
  }

  private static void shuffle(Comparable[] a) {
  int N = a.length;
  Random rand = new Random();
  for (int i=0; i<N; i++) {
    int r = rand.nextInt(i+1); // random element
    swap(a, i, r); // swap current element with the randomly chosen one
  }
}

  /**
   * returns true when the item v is less than item w
   */
  private static boolean less(Comparable v, Comparable w) {
    return v.compareTo(w) < 0; // (-1 v<w || 0 v=w || 1 v>w : These are the values from compareTo)
  }

  private static void show(Comparable[] a) {
    for (int i=0; i<a.length; i++)
      println(a[i] + " ");
  }

  public static boolean isSorted(Comparable[] a) {
    println("Number of items sorted: "+a.length);
    for (int i=1; i<a.length; i++)
      if (less(a[i], a[i-1]))
        return false;
    return true;
  }
}
/**
 * Timer for measuring computation time for algo
 */
public class Timer {
  private final long start;

  /**
   * Initializes a new stopwatch.
   */
  public Timer() {
    start = System.currentTimeMillis();
  } 


  /**
   * Returns the elapsed CPU time (in seconds) since the stopwatch was created.
   *
   * @return elapsed CPU time (in seconds) since the stopwatch was created
   */
  public double elapsedTime() {
    long now = System.currentTimeMillis();
    return (now - start) / 1000.0f;
  }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#000000", "--hide-stop", "QuickSort" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
